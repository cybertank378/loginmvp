package com.lowencon.login

interface LoginPresenter {

    fun validateCredentials(username: String, password: String)

    fun onDestroy()


}