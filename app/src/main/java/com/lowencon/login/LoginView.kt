package com.lowencon.login

interface LoginView {


    fun showProgress()

    fun hideProgress()

    fun setUserNameError()

    fun setPasswordError()

    fun navigateToHome()
}