package com.lowencon.main

interface MainViewPresenter {

    fun onResume()

    fun onItemClick(position: Int)

    fun onDestroy()



}